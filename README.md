mp3player [![endorse](https://api.coderwall.com/tarcnux/endorsecount.png)](https://coderwall.com/tarcnux)
=========

Creating an MP3 Player with HTML5 by Mark Lassoff

[Udemy Course Webpage](https://www.udemy.com/creating-an-mp3-player-with-html5/)
