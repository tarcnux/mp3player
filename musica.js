var arquivoXml = '<?xml version="1.0" encoding="UTF-8"?> \
<lista>\
    \
    <musica>\
        <titulo>Moonlight</titulo>\
        <compositor>The Piano Guys</compositor>\
        <tempo>3:26</tempo>\
        <arquivo>moonlight.mp3</arquivo>\
    </musica>\
    \
    <musica>\
        <titulo>More than Words</titulo>\
        <compositor>The Piano Guys</compositor>\
        <tempo>3:55</tempo>\
        <arquivo>more_than_words.mp3</arquivo>\
    </musica>\
\
    <musica>\
        <titulo>Michael meets Mozart</titulo>\
        <compositor>The Piano Guys</compositor>\
        <tempo>5:16</tempo>\
        <arquivo>mozart_by_piano_guys.mp3</arquivo>\
    </musica>\
    \
    <musica>\
        <titulo>O Fortuna (Carmina Burana)</titulo>\
        <compositor>The Piano Guys</compositor>\
        <tempo>3:19</tempo>\
        <arquivo>o_fortuna.mp3</arquivo>\
    </musica>\
    \
    <musica>\
        <titulo>Rolling in the deep</titulo>\
        <compositor>The Piano Guys</compositor>\
        <tempo>3:52</tempo>\
        <arquivo>rolling_in_the_deep.mp3</arquivo>\
    </musica>    \
    \
</lista>';